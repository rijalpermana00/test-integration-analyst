# Test Integration Analyst - Rijal Permana

Jika melihat dari diagram yang telah diberikan, ada beberapa stack tech yang digunakan, 

### 1. OpenShift Container Platform dan Java Springboot
Sistem ini dibangun dengan menggunakan bahasa pemrograman Java dengan menggunakan Framework Springboot dan menerapkan Microservice Architecture dengan container based system yang dideploy menggunakan Openshift Container Platform. Infrastruktur ini memungkinkan untuk di scaling sesuai dengan kebutuhan secara vertical ataupun horizontal.

### 2. Service Worker
Service worker adalah script yang akan berjalan di background, ada banyak fungsi dari service worker ini, contohnya sebagai proxy untuk menghubungkan antara aplikasi dengan sistem lain seperti caching system dan push notification

### 3. Kibana
Kibana merupakan platform pendukung yang biasa digunakan untuk menampilkan visualisasi dan analisis data. Service yang digunakan pada kibana biasanya adalah Elastic Search yang mengumpulkan data log dari service yang berjalan di dalam OCP.

### 4. Jenkins
Jenkins adalah sistem automasi yang biasanya digunakan dalam tahap building, testing dan deploying dengan menerapkan CI/CD. Jenkins dapat melakukan deploy aplikasi secara otomatis sesuai dengan aturan yang telah ditentukan. Jenkins biasanya diintegrasikan dengan Git sistem, yang dimana ketika ada push/merge request, Jenkins akan mendeploy aplikasi tersebut sesuai dengan rulesnya.

### 5. SOA - Service Oriented Architecture
SOA merupakan middleware yang menghubungkan antara sistem yang digunakan dalam konstelasi sistem. SOA dapat mengintegrasikan antar sistem dengan menggunakan banyak cara seperti menggunakan Rest API, SOAP, 

### 6. Core Banking System
Core banking sistem digunakan sebagai sumber data yang valid dan dapat digunakan untuk melakukan transaksi seperti manajemen nasabah, manajemen rekening, payment, transfer dan yang lainnya.

### 7. Open API
Open API Specification adalah platform dokumentasi API yang standar digunakan. Dokumentasi mencakup End Point API, authentication API, fungsi, cara penggunaan, inputan apa saja yang diperlukan, output apa saja yang mungkin didapatkan oleh user dan masih banyak yang lainnya.

-----

### TLDR: 

Secara singkatnya, aplikasi yang dibuat dengan menggunakan Java Springboot di deploy di dalam OCP dengan menggunakan Jenkins sebagai sistem CI/CD, lalu menggunakan Kibana elastic search untuk memonitoring dan menganalisa data log dari sistem tersebut. Service worker digunakan untuk kebutuhan pengiriman dan penerimaan notifikasi, dan memperingan load aplikasi dengan caching system. masing masing service yang telah di deploy dalam OCP ini di integrasikan dengan Core Banking System dengan mengunakan SOA yang akan didokumentasikan dengan Open API specification sehingga pengembang service dapat mengetahui dengan pasti API apa saja yang ada dan dapat digunakan.